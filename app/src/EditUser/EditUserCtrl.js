'use strict';

userApp.controller('EditUserCtrl', function ($scope, $routeParams, UsersService) {
  $scope.newDataUser = {};

  $scope.userId = $routeParams['userId'];
  $scope.editUser = function (myUser, id) {
    $scope.editSuccess = false;

    UsersService.editUser(myUser, id).then(function (response) {
      $scope.newDataUser = {};
      $scope.editSuccess = true
    })
  }
});
