'use strict';

userApp.controller('UserListCtrl', function ($scope, UsersService, PostsService, $q) {
  /* первая часть с лоадером */
/*
  UsersService.getUsers().then(function (response) {
    $scope.users = response.data;
    $scope.userListLoaded = true
  });

  PostsService.getPosts().then(function (response) {
    $scope.posts = response.data;
    $scope.postListLoaded = true;
  });
*/

 //дополнительное задание
  var usersPromise =  UsersService.getUsers();
  var postsPromise =  PostsService.getPosts();

  $q.all([usersPromise, postsPromise]).then(function(data) {
          $scope.users = data[0].data;
          $scope.posts = data[1].data;
          $scope.allDataLoaded = true;
      }
  );


/*   UsersService.getUsers().then(function (response) {
    $scope.users = response.data
    return PostsService.getPosts()
  }).then(function (response) {
    $scope.posts = response.data
  }) */

});
